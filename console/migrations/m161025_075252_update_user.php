<?php

use yii\db\Migration;

class m161025_075252_update_user extends Migration
{
    public function up()
    {
        $this->addColumn("{{%user}}", 'name', $this->string()->after('username')->notNull()->defaultValue("no name")->comment('Name User'));
        $this->dropColumn("{{%user}}", 'email');
    }

    public function down()
    {
        $this->addColumn("{{%user}}", 'email', $this->string()->unique());
        $this->dropColumn("{{%user}}", 'name');
        return true;
    }

}
