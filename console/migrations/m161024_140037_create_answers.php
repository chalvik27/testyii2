<?php

use console\models\BaseMigration;

class m161024_140037_create_answers extends BaseMigration
{
    public function up()
    {
        $this->createTable('{{%answers}}', [
            
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer()->notNull(),
            'question_id'   => $this->integer()->notNull(),
            'content'       => $this->text(),
            'created_at'    => $this->timestamp()->defaultValue(null),
            
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%answers}}');
        return true;
    }

}
