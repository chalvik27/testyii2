<?php

use console\models\BaseMigration;


class m161024_140029_create_questions extends BaseMigration
{
    public function up()
    {
        $this->createTable('{{%questions}}', [
            
            'id'           => $this->primaryKey(),
            'user_id'      => $this->integer()->notNull(),
            'content'      => $this->text(),
            'created_at'   => $this->timestamp()->defaultValue(null),
            
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%questions}}');
        return false;
    }
    
}
