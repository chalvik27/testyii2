<?php

namespace frontend\controllers;

use Yii;
use common\models\Answers;
use common\models\Questions;
use frontend\models\search\AnswersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;


/**
 * AnswersController implements the CRUD actions for Answers model.
 */
class AnswersController extends Controller
{
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'actions' => ['create'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],            
//        ];
//    }


    /**
     * Creates a new Answers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($question_id)
    {
        if (Yii::$app->request->isAjax) {
            
            if (!( ($question = Questions::findOne($question_id)) !== null)) {
                throw new NotFoundHttpException('Question not found ');
            }
            
            $model = new Answers();
            
            if ($model->load(Yii::$app->request->post())) { 
                
                $model->user_id = Yii::$app->user->identity->id;
                $model->question_id = $question_id;
                
                if($model->save()) {
                    $model->refresh();
                    return  $this->renderAjax('/questions/_answer', [
                        'model' => $model,
                    ]);
                } 
                
            }
                
                return  $this->renderAjax('create', [
                    'model' => $model,
                ]);
                
            
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');            
        }    
    }


    /**
     * Finds the Answers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Answers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Answers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
