
$("#add_answer").click(function(e){
   e.preventDefault();
   var  button = $(this);
   var url = button.attr("href");
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'html',
        success: (function(data) {
            button.after(data).show();
            button.hide();
            
        }),
        error: (function(){
            alert ("Ошибка запроса ");
        })    
    })
    
});

$("#questions-view").on("submit","form",function(e){
   e.preventDefault();
   
   $(this).hide();
   
   var url = $(this).attr("action");
   var data =  $(this).serialize();
   
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: 'html',
        success: (function(data) {
            
            $("#answers").after(data);
            $("#add_answer").show();
        }),
        error: (function(){
            alert ("Ошибка запроса ");
            $("#add_answer").show();
        })
    });
    
});
