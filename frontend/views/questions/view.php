<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */

$this->title = "Question number ".$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div  id="questions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=$model->content?>

    <?=Html::a(" Добавить ответ ", 
            ['/answers/create','question_id'=>$model->id],
            ["class"=>"btn btn-primary","id"=>"add_answer"]
            );?>
    
    
    <ul id="answers" class="list-group">
    <?php if (isset ($model->answers)): ?>
        <?php foreach ($model->answers as $answer):?>
            <?=$this->render("_answer",[
                'model'=>$answer
            ]);?>
        <?php endforeach;?>
    <?php endif;?>
    </ul>
        

</div>
