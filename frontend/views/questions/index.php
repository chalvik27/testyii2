<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(!Yii::$app->user->isGuest): ?>
    <p>
        <?= Html::a('Create Questions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->name;
                },
                'filter'    => Select2::widget([
                    'model' => $searchModel,
                    'data' => \common\models\User::find()->select(['name', 'id'])->indexBy('id')->column(),
                    'attribute' => 'user_id',
                    'options' => [
                            'placeholder' => 'Выберите пользователя',
                            'id' => 'user_id',
                            ],
                    'pluginOptions' => [
                            'allowClear' => true,
                    ]
                ]),
            ],                
            
            'content:html',
            'created_at',

       [
           'class' => \yii\grid\ActionColumn::className(),
           'template'=>'{view}',
       ],            
            
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
