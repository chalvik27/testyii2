<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;



/**
 * This is the model class for table "answers".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property string $content
 * @property string $created_at
 */
class Answers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answers';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => new Expression('NOW()'),
                'attributes' => array(
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ),
            ],
        ];
    }     
    
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id'], 'required'],
            [['user_id', 'question_id'], 'integer'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'user_id'       => 'User ID',
            'question_id'   => 'Question ID',
            'content'       => 'Content',
            'created_at'    => 'Created At',
        ];
    }
    
    /**
     * 
     * @return object
     */
    public function getQuestion(){
        
        return $this->hasOne(Questions::className(), ['id' => 'question_id']);
        
    }    
    
    /**
     * 
     * @return object
     */
    public function getUser(){
        
        return $this->hasOne(User::className(), ['id' => 'user_id']);
        
    }     
    
}
