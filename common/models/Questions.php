<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $content
 * @property string $created_at
 */
class Questions extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => new Expression('NOW()'),
                'attributes' => array(
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ),
            ],
        ];
    }     
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'user_id'       => 'User ID',
            'content'       => 'Content',
            'created_at'    => 'Created At',
        ];
    }
    
    /**
     * 
     * @return array objects
     */
    public function getAnswers(){
        
        return $this->hasMany(Answers::className(), ['question_id' => 'id']);
        
    }     

    /**
     * 
     * @return object
     */
    public function getUser(){
        
        return $this->hasOne(User::className(), ['id' => 'user_id']);
        
    }     

    
    public static function find()
    {
        return new QuestionQuery(get_called_class());
    }    
    
    
}



class QuestionQuery extends ActiveQuery
{
        
    
}
